({
	doInit : function(component, event, helper) {
		var action = component.get("c.getContactList");
        action.setParams({ campaignId : cmp.get("v.recordId")});
        action.setCallback(this, function(responce){
        	var state = response.getState();
            if(state  === "SUCCESS"){
                component.set("v.contactList", response.getReturnValue());
            }			
        }
	}
})