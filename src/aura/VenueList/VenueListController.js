//final last mine new ctrl 
({
    searchKeyChange: function(component, event, helper) {
        debugger;
        component.set("v.hadselected", true);
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findVenueByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {
            component.set("v.venuelist", a.getReturnValue());
            helper.onSelect(component, event);
        });
        $A.enqueueAction(action);
        
 

    },
    handleChanged: function(component, evt, helper) {
        debugger;
        var checkedList = component.find("checked");
        var result = checkedList.get("v.value");
        component.set("v.checkboxsel", result);
        component.set("v.selectedsubvenue", null);
    },
    selectonCheck: function(component, event, helper) {
        debugger;
        var venuelist = component.get("v.venueRecordLimitList");
        component.set("v.selectedsubvenue", venuelist);

        var a = event.currentTarget.checked;

        var b = event.currentTarget.dataset.record;

        var selectedsubvenues = component.get("v.juncobj");


        if (b != null || b != undefined) {
            if (a === true) {
                selectedsubvenues.push(b);
            }
        }
        if (b != null || b != undefined) {
            if (a === false) {
                selectedsubvenues.splice(selectedsubvenues.indexOf(b), 1);
            }
        }

        component.set("v.juncobj", selectedsubvenues);

    },
    selectonRadio: function(component, event, helper) {
        debugger;
        //var temp= [];
        //component.set("v.juncobj",temp);
        $('table tr').click(function() {
            $(this).find('input:radio').prop('checked', true);
            var venueId = $(this).find('input:radio').attr('value');
            var selectedsubvenues = component.get("v.juncobj");
            if (venueId != null || venueId != undefined) {

                selectedsubvenues.push(venueId);

            }
            component.set("v.juncobj", selectedsubvenues);
        });

        var checkboxesChecked = component.get("v.juncobj");
        component.set("v.juncobj", event.target.value);
        helper.handleChange(component, event);
        component.set('v.TabId', "venueid");


    },
    handleChangeSubvenue: function(component, event, helper) {
        debugger;
        helper.handleChange(component, event, helper);

    },

    clickOnsubVenue: function(component, event, helper) {
        debugger;
     $('table tr').click(function() {
            debugger;
            $(this).find('input:radio').prop('checked', true);
            var venueId = $(this).find('input:radio').attr('value');
            var selectedsubvenues = component.get("v.juncobj");
            if (venueId != null || venueId != undefined) {

                selectedsubvenues.push(venueId);

            }
        });

        var selectedItem = event.currentTarget;
        var wholeRecord = selectedItem.dataset.record;
        var action = component.get("c.getsubVenueList");
        var checkboxtrue = component.get("v.checkboxsel");
        action.setParams({ "subvenueId": wholeRecord });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var subVenueList = response.getReturnValue();
            if (subVenueList == '') {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "Error!",
                    "title": "Info!",
                    "message": "There is no Subvenue available for selected Venue",
                });
                toastEvent.fire();
                event.preventDefault();

            } else if (state === "SUCCESS") {
                if (checkboxtrue == false) {
                    component.set("v.juncobj", []);
                }


                component.set("v.selectedsubvenue", []);
                component.set("v.selectedsubvenue", subVenueList);
            }
        });
        $A.enqueueAction(action);
    },
    saveNnext: function(component, event, helper) {
        debugger;
        var checkboxesChecked = component.get("v.juncobj");
        var selectedsubvenues = component.get("v.selectedsubvenue");

        var venuelist = component.get("v.venueRecordLimitList");

        if (checkboxesChecked == '' || checkboxesChecked == undefined) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "Error!",
                "title": "Error!",
                "message": "Please select sub venues",
            });
            toastEvent.fire();
            event.preventDefault();
        } else {
            component.set('v.venueIds', "");
            component.set('v.venueIds', checkboxesChecked);
            component.set('v.TabId', "venuelayid");
            //helper.resetSubVenues(component, event, helper);
            var cmpEvent = $A.get("e.c:componentCommunicationEvent");
            cmpEvent.setParams({
                "target": "A component event fired me. ",
                "targetIDs": "sample"
            });
            cmpEvent.fire();
        }
    },

    onClickfirstPage: function(component, event, helper) {
        component.set("v.currentPageNumber", 1);
        component.set("v.pageNumber", 1);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        }
    },
    onClickprevPage: function(component, event, helper) {
        var pageN = component.get("v.pageNumber");
        pageN--;
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber") - 1, 1));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        } else if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        }
    },
    onClicknextPage: function(component, event, helper) {
        debugger;
        var pageN = component.get("v.pageNumber");
        pageN++;
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber") + 1, component.get("v.maxPageNumber")));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.onClickRenderPage(component);
        } else if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.onClickRenderPage(component);
        }

    },
    onClicklastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
        component.set("v.pageNumber", component.get("v.maxPageNumber"));
        if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.onClickRenderPage(component);
        }
    },
    applyCSS: function(cmp, event) {
        debugger;
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
        var toggleText = cmp.find('text');
        $A.util.toggleClass(toggleText[inputsel], "toggle");
    },
    editRecord: function(component, event, helper) {
        debugger;

        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;

        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": inputsel
        });
        editRecordEvent.fire();
        event.preventDefault();
    },
    handleApplicationEvent: function(component, event, helper) {
        var target = event.getParam("target");

    },
    cancel: function(component, event, helper) {
        debugger;
        component.set('v.TabId', "eventid");

    },
    checkradiotrue: function(component, event, helper) {
        debugger;
        $('table tr').click(function() {
            $(this).find('input:radio').prop('checked', true);
        });
    }

})