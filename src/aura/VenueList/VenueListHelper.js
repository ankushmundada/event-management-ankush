//final last mine new helper
({
  onClickRenderPage: function(component) {
        debugger;
    var records = component.get("v.venuelist"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*5, pageNumber*5);
          component.set("v.venueRecordLimitList", pageRecords);
  },
    onSelect:function (component){
        
        component.set("v.pageNumber",1);
        debugger;
              var resultEmp = component.get("v.venuelist");
                component.set("v.totalRecordsCount",resultEmp.length);
                component.set("v.oppRecordList",resultEmp);
                component.set("v.maxPage", Math.floor((resultEmp.length+9)/10));
                component.set("v.maxPageNumber", Math.floor((resultEmp.length+9)/10));
                debugger;
              if(resultEmp === null || resultEmp === undefined || resultEmp.length ===  0){
                component.set("v.pageNumber",0); 
                component.set("v.firstclick",true);
                component.set("v.prevclick",true);
                component.set("v.nextclick",true);
                component.set("v.lastclick",true);  
                }
                this.onClickRenderPage(component);    //calling helper's own method 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",false);
                 component.set("v.lastclick",false);       
              if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){ 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",true);
                 component.set("v.lastclick",true); 
                }    
      },
    handleChange : function(component,event,helper){
        debugger;
        
        //component.set("v.juncobj",selectedsubvenues);
        
        //event.currentTarget.checked
        
        //event.currentTarget.dataset.record
        
        var venuelist = component.get("v.selectedsubvenue");
        component.set("v.selectedsubvenue",venuelist);
        
       	var a = event.currentTarget.checked;
        
        var b = event.currentTarget.dataset.record;
        
        var selectedsubvenues = component.get("v.juncobj");
        //var temp= [];
       	//component.set("v.juncobj",temp);
        
        if(b != null || b != undefined){
            if(a === true){
                selectedsubvenues.push(b);
            } 
        }
        if(b != null || b != undefined){
            if(a === false){
                selectedsubvenues.splice(selectedsubvenues.indexOf(b),  1);
            } 
        }
      
        component.set("v.juncobj",selectedsubvenues);
    },
    
    
     resetSubVenues: function(component,event,helper) {
		var subVenueCheckedList = component.find("lastchecked");
        if (subVenueCheckedList == null || subVenueCheckedList == undefined) {
            return;
        }
        
        for(var sub =0; sub < subVenueCheckedList.length; sub++) {
            subVenueCheckedList[sub].set("v.value", false);
        }
        
    }
  
    
})