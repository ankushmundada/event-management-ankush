({
	onClickRenderPage: function(component) {
        debugger;
		var records = component.get("v.oppRecordList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
        	component.set("v.oppRecordLimitList", pageRecords);
	},
    onSelect:function (component){
        
        component.set("v.pageNumber",1);
        debugger;
        var aCheck = component.get("v.currentListView");
        var selected = component.find("levels").get("v.value");
      	var act = component.get("c.opprecordListGet");
        act.setParams({ SelectedListView : selected});
     	act.setCallback(this, function(response){
            debugger;
        var state = response.getState();
        	if(state === "SUCCESS"){
            	var resultEmp = response.getReturnValue(); 
                component.set("v.totalRecordsCount",resultEmp.length);
                component.set("v.oppRecordList",resultEmp);
                component.set("v.maxPage", Math.floor((resultEmp.length+9)/10));
				console.log('records  ',response.getReturnValue());
                component.set("v.maxPageNumber", Math.floor((resultEmp.length+9)/10));
                console.log('maxPageNumber  ',component.get("v.maxPageNumber"));
                debugger;
              if(resultEmp === null || resultEmp === undefined || resultEmp.length ===  0){
                component.set("v.pageNumber",0); 
                component.set("v.firstclick",true);
                component.set("v.prevclick",true);
                component.set("v.nextclick",true);
                component.set("v.lastclick",true);  
                }
                this.onClickRenderPage(component);		//calling helper's own method 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",false);
                 component.set("v.lastclick",false);       
              if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){ 
                 component.set("v.firstclick",true);
                 component.set("v.prevclick",true);
                 component.set("v.nextclick",true);
                 component.set("v.lastclick",true); 
                }    
			}
       	});
      	$A.enqueueAction(act);
        
        
    }
})