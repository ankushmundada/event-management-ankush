public virtual class SobjectControllerExtension {
    
    public static final string EXCEPTION_MESSAGE = 'Insufficient Access Permission';


    public class ControllerException extends Exception {}

    public Id controllerId { get; set; }

    public SobjectControllerExtension() {
        this.controllerId = null;
    }

    public SobjectControllerExtension(Id sobjectId) {
        system.assert(sobjectId != null, 'controller is null');
        this.controllerId = sobjectId;
    }

    public SobjectControllerExtension(ApexPages.StandardController controller) {
        system.assert(controller != null, 'controller is null');
        this.controllerId = controller.getId();
    }

    public SobjectControllerExtension(ApexPages.StandardSetController controller) {
        system.assert(controller != null, 'controller is null');
        this.controllerId = getParam('id');
    }

    public virtual PageReference addMessage(ApexPages.Severity severity, String message) {
        ApexPages.addMessage(new ApexPages.Message(severity, message));
        return null;
    }

    public virtual PageReference addInfoMessage(String message) {
        return addMessage(ApexPages.Severity.Info, message);
    }

    public virtual PageReference addWarningMessage(String message) {
        return addMessage(ApexPages.Severity.Warning, message);
    }

    public virtual PageReference addErrorMessage(String message) {
        return addMessage(ApexPages.Severity.Error, message);
    }

    public virtual PageReference addErrorMessageAndRollback(Savepoint lastSavepoint, String message) {
        Database.rollback(lastSavepoint);
        return addMessage(ApexPages.Severity.Error, message);
    }

    public virtual PageReference addErrorMessageAndRollback(Savepoint lastSavepoint, Exception pException) {
        Database.rollback(lastSavepoint);

        Boolean isIgnored01 = pException.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION');
        Boolean isIgnored02 = pException.getMessage().contains('FIELD_INTEGRITY_EXCEPTION');
        Boolean isIgnored03 = pException.getMessage().contains('REQUIRED_FIELD_MISSING');
        return isIgnored01 || isIgnored02 || isIgnored03 ? null : addErrorMessage(pException);
    }

    public virtual PageReference addErrorMessage(Exception pException) {
        return addMessage(ApexPages.Severity.Error, pException.getTypeName() + ': ' + pException.getMessage());
    }

    public virtual PageReference addErrorMessageAndRollbackWithValidationErrors(Savepoint savepoint, Exception pException) {
        Database.rollback(savepoint);

        if (pException.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
            return addMessage(ApexPages.Severity.Error, pException.getDMLMessage(0));
        }
        return addMessage(ApexPages.Severity.Error, pException.getTypeName() + ': ' + pException.getMessage());
    }

    public virtual Boolean hasWarningMessages() {
        return ApexPages.hasMessages(ApexPages.Severity.Warning);
    }

    public virtual Boolean hasErrorMessages() {
        return ApexPages.hasMessages(ApexPages.Severity.Error);
    }

    public virtual PageReference redirectTo(PageReference pageReference) {
        return redirectTo(pageReference, true);
    }

    public virtual PageReference redirectTo(PageReference pageReference, Boolean includeId) {
        if (includeId == true && getId() != null) {
            pageReference.getParameters().put('id', controllerId);
        }
        pageReference.setRedirect(true);
        return pageReference;
    }

    public virtual String getParam(String paramName, String defaultValue) {
        system.assert(ApexPages.currentPage() != null, 'ApexPages.currentPage() == null');
        system.assert(ApexPages.currentPage().getParameters() != null, 'ApexPages.currentPage().getParameters() == null');
        String param = ApexPages.currentPage().getParameters().get(paramName);
        param = param == null ? defaultValue : param;
        if (param == null) {
            return param;
        } else {
            return String.escapeSingleQuotes(param);
        }
    }

    public virtual String getParam(String paramName) {
        return getParam(paramName, '');
    }

    public virtual void putParam(String paramName, String paramData) {
        system.assert(ApexPages.currentPage() != null, 'ApexPages.currentPage() == null');
        system.assert(ApexPages.currentPage().getParameters() != null, 'ApexPages.currentPage().getParameters() == null');
        ApexPages.currentPage().getParameters().put(paramName, paramData);
    }

    public virtual Id getId() {
        return controllerId;
    }

    public virtual PageReference initializeSafe() {
        try {
            return initialize();
        } catch (Exception systemException) {
            return addErrorMessage(systemException);
        }

        return null;
    }

    public virtual PageReference initialize() {
        return null;
    }

    public static Set<String> getFieldStringSet(Sobject[] sobjectRefList, Schema.SobjectField sobjectField) {
        Set<String> fieldSet = new Set<String> {};

        if (sobjectRefList != null && sobjectRefList.size() != 0 && sobjectField != null) {
            for(Sobject sobjectRef : sobjectRefList) {
                fieldSet.add((String) sobjectRef.get(sobjectField));
            }
        }

        return fieldSet;
    }

    public static Map<Schema.SObjectType,SobjectSecurityPermission> SobjectSecurityPermissionMap = new Map<Schema.SObjectType,SobjectSecurityPermission>();
    public static Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

    public class SobjectSecurityPermission {
        public Schema.SObjectType sobjectType;
        public Map<String,SobjectFieldSecurityPermission> sobjectFieldSecurityPermissionMap;
        public Boolean isDeleteAccess;
        public Boolean isUpdateableAccess;

        public SobjectSecurityPermission(Schema.SObjectType sobjectType, Schema.DescribeSObjectResult describeResult) {
            this.sobjectType = sobjectType;
            isDeleteAccess = describeResult.isDeletable();
            isUpdateableAccess = describeResult.isUpdateable();

            this.sobjectFieldSecurityPermissionMap = new Map<String,SobjectFieldSecurityPermission>();
            RetrieveFieldPermissions(describeResult);
        }

        public void RetrieveFieldPermissions(Schema.DescribeSObjectResult describeResult) {
            Map<String, Schema.SObjectField> sobjectFieldsMap = describeResult.Fields.getMap();

            for (Schema.SObjectField sobjectField: sobjectFieldsMap.values()) {
               String fieldName = sobjectField.getDescribe().getName();
               if (sobjectFieldSecurityPermissionMap.get(fieldName) == null) {
                   SobjectFieldSecurityPermission FieldPermission = new SobjectFieldSecurityPermission(sobjectField,sobjectField.getDescribe());
                   this.sobjectFieldSecurityPermissionMap.put(fieldName,FieldPermission);
               }
            }
        }
    }

    public class SobjectFieldSecurityPermission {
        public Schema.SObjectField sobjectField;
        public Boolean isReadAccess;
        public Boolean isCreateAccess;
        public Boolean isUpdateAccess;
        public Boolean isPermissionAccess;
        public Boolean isFormulaField;

        public SobjectFieldSecurityPermission(Schema.SObjectField sobjectField,Schema.DescribeFieldResult describeResult) {
           this.sobjectField = sobjectField;
           this.isReadAccess = describeResult.isAccessible();
           this.isCreateAccess = describeResult.isCreateable();
           this.isUpdateAccess = describeResult.isUpdateable();
           this.isPermissionAccess = describeResult.isPermissionable();
           this.isFormulaField = describeResult.isCalculated();
        }
    }

    public static SobjectSecurityPermission getSobjectSecurityPermission(Schema.SObjectType sobjectType) {
        if (SobjectSecurityPermissionMap.get(sobjectType) != null) {
           return SobjectSecurityPermissionMap.get(sobjectType);
        }

        SobjectSecurityPermission sobjectPermission = new SobjectSecurityPermission(sobjectType,sobjectType.getDescribe());
        SobjectSecurityPermissionMap.put(sobjectType,sobjectPermission);
        return sobjectPermission;
    }

    public static Boolean isSobjectDeletable(Schema.SObjectType sobjectType) {
        if (sobjectType == null) {
            return false;
        }
        SobjectSecurityPermission objectPermission = SobjectSecurityPermissionMap.get(sobjectType);

        if (objectPermission == null) {
            objectPermission = getSobjectSecurityPermission(sobjectType);
        }

        if (objectPermission == null) {
            return false;
        }

        return objectPermission.isDeleteAccess;
    }

    public static Boolean isSobjectUpdateable(Schema.SObjectType sobjectType) {
        return sobjectType == null ? false : getSobjectSecurityPermission(sobjectType).isUpdateableAccess;
    }

    public static Boolean isSobjectFieldsAccessible(Schema.SObjectType sobjectType, String[] fields) {
        if (sobjectType == null || fields == null || fields.size() == 0) {
            return false;
        }

        SobjectSecurityPermission objectPermission = SobjectSecurityPermissionMap.get(sobjectType);


        if (objectPermission == null) {
            objectPermission = getSobjectSecurityPermission(sobjectType);
        }

        for(String sfield :fields) {
            SobjectFieldSecurityPermission fieldPermission = objectPermission.sobjectFieldSecurityPermissionMap.get(sfield);

            if (fieldPermission == null) {
                return false;
            }

            if (fieldPermission.isPermissionAccess == true && fieldPermission.isReadAccess == false) {
                return false;
            }
        }
        return true;
    }

    public static Boolean isSobjectFieldsCreatable(Schema.SObjectType sobjectType, String[] fields) {
        if (sobjectType == null || fields == null || fields.size() == 0) {
            return false;
        }

        SobjectSecurityPermission objectPermission = SobjectSecurityPermissionMap.get(sobjectType);


        if (objectPermission == null) {
            objectPermission = getSobjectSecurityPermission(sobjectType);
        }

        for(String sfield :fields) {
            SobjectFieldSecurityPermission fieldPermission = objectPermission.sobjectFieldSecurityPermissionMap.get(sfield);

            if (fieldPermission == null) {
                return false;
            }

            if (fieldPermission.isFormulaField == true) {
                continue;
            }

            if (fieldPermission.isPermissionAccess == true && fieldPermission.isCreateAccess == false) {
                return false;
            }
        }
        return true;
    }

    public static Boolean isSobjectFieldsUpdatable(Schema.SObjectType sobjectType, String[] fields) {
        if (sobjectType == null || fields == null || fields.size() == 0) {
            return false;
        }

        SobjectSecurityPermission objectPermission = SobjectSecurityPermissionMap.get(sobjectType);


        if (objectPermission == null) {
            objectPermission = getSobjectSecurityPermission(sobjectType);
        }

        for(String sfield :fields) {
            SobjectFieldSecurityPermission fieldPermission = objectPermission.sobjectFieldSecurityPermissionMap.get(sfield);

            if (fieldPermission == null) {
                return false;
            }

            if (fieldPermission.isFormulaField == true) {
                continue;
            }

            if (fieldPermission.isPermissionAccess == true && fieldPermission.isUpdateAccess == false) {
                return false;
            }
        }
        return true;
    }

    // Check if oldValue and newValue are different
    // will return true if value has been changed
    public static Boolean isValueChanged(String oldValue, String newValue) {
        return !blank(oldValue).equalsIgnoreCase(blank(newValue));
    }


    public static String blank(String value) {
         return value == null ? '' : value;
    }

}