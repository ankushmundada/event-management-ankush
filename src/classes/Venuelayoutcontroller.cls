public class VenueLayoutController { 
    

    public static String LAYOUT_TYPE_ROUND = 'Round';
    public static String LAYOUT_TYPE_SQUARE = 'Square';
    public static String LAYOUT_TYPE_AUDIENCE = 'Audience';
    public static String LAYOUT_TYPE_CLASSROOM = 'Classroom';
    public static String LAYOUT_TYPE_BOOTH = 'Booth';


    @AuraEnabled  
    public static List<SelectedVenueWrapper> getEventVenues(List<String> venueList, Id recordId) {
        system.debug('venueList'+venueList);
        system.debug('recordId'+recordId);
        if (recordId == null) {
            return null;
        }
        
        Map<Id, Boolean> venueToLayoutPresentMap = new Map<Id, Boolean>();
         Map<String, String> eventVenueIdPresentMap = new Map<String, String>();
        List<Event_Venue__c> eventVenueList = [ SELECT Id,Venue__c
                                                     , Venue_Layout__c
                                                  FROM Event_Venue__c 
                                                 WHERE Campaign__c = :recordId 
		];
        
        Set<String> venueIds = new Set<String>(venueList);
        
        for(Event_Venue__c eventVenue :eventVenueList) {
            Boolean isLayoutPresent = eventVenue.Venue_Layout__c == null ? false : true;
            venueToLayoutPresentMap.put(eventvenue.Venue__c, isLayoutPresent);
            eventVenueIdPresentMap.put(eventVenue.Venue__c, eventVenue.Id);
            venueIds.add(eventVenue.Venue__c);
        }
        
       
        /*List<Venue__c> options = [select Id,Name,ParentVenue__r.Name,Seating_Capacity__c,Type__c from Venue__c where id = :venueList];
system.debug('optionsssss'+options);*/
        
        List<SelectedVenueWrapper> selectedVenueWrapperList = new List<SelectedVenueWrapper>();
        
        for (Venue__c venue :[SELECT Id
                                   , Name
                                   , ParentVenue__r.Name
                                   , Seating_Capacity__c
                                   , Type__c 
                                FROM Venue__c 
                               WHERE id = :venueIds ]) {
                                   
            SelectedVenueWrapper selectedVenueWrapper = new SelectedVenueWrapper();
            selectedVenueWrapper.Id = venue.Id;
            selectedVenueWrapper.Name = venue.Name;
            selectedVenueWrapper.capacity = String.valueOf(venue.Seating_Capacity__c);
            selectedVenueWrapper.Type = venue.Type__c;
            selectedVenueWrapper.EventVenueId = eventVenueIdPresentMap.containsKey(venue.Id) ? eventVenueIdPresentMap.get(venue.Id) : null;
			if(!venueToLayoutPresentMap.isEmpty()) {
                selectedVenueWrapper.IsCustomized = venueToLayoutPresentMap.containsKey(venue.Id) ? venueToLayoutPresentMap.get(venue.Id) : false;
            }
                                   
            selectedVenueWrapperList.add(selectedVenueWrapper);
        }
        
        return selectedVenueWrapperList;
    }
    /*
	 * method name change -> saveEventVenueLayout
	 * radiodata -> venueLayoutId
	*/
    
   @AuraEnabled 
    public static Event_venue__c saveEventVenueLayout(String venueLayoutId, Id recordId, String venueid) {
        if (recordId == null) {
            return null;
        }
        
        Campaign campobj = [ SELECT Start_Date__c
                                  , End_Date__c
                                  , Id 
                               FROM Campaign 
                              WHERE Id = :recordId ];
        
        List<Event_venue__c> eventobj = new List<Event_venue__c>();
        
        eventobj = [ SELECT Id
                          , Venue_Layout__c
                          , campaign__r.Start_Date__c
                          , campaign__r.End_Date__c 
                       FROM Event_venue__c
                      WHERE Venue_Layout__c = :venueLayoutId 
                        AND ((campaign__r.start_date__c >= :campobj.Start_Date__c and campaign__r.end_date__c <=:campobj.End_Date__c) 
                              OR
                     		 (campaign__r.start_date__c <=:campobj.Start_Date__c and campaign__r.end_date__c >= :campobj.End_Date__c)
                            ) 
                      LIMIT 1];
 
        Event_Venue__c  eventVenue = new Event_Venue__c();
        eventVenue.Campaign__c = recordId;
     	eventVenue.Venue__c = venueid;	
        
        if (eventobj.size() == 0) {
            eventVenue.Venue_Layout__c = venueLayoutId;
            
            return createEventVenue(eventVenue);
        }
        
        Venue_Layout__c venueLayout = getVenueLayout(eventobj[0].Venue_Layout__c); 
        eventVenue.Venue_Layout__c = venueLayout.Id;
        
        return createEventVenue(eventVenue);
    }

    /* Change name getpickval -> getLayoutTypes()
     * It retrieves Layout from Layout custom metadata type
     * Change - options -> layoutTypeList
	*/
    @AuraEnabled
    public Static List<Layout__mdt> getLayoutTypes()
    {
		List<Layout__mdt> layoutTypeList = [ SELECT DeveloperName
                                               FROM Layout__mdt ];
        
        return layoutTypeList;
    }

    /* Change name getpickvaltype -> getCustomLayoutTypes()
     * It retrieves custom Layout from Layout custom metadata type
     * Change - options -> customLayoutTypeList
	*/
    @AuraEnabled
    public Static  List<Custom__mdt> getCustomLayoutTypes()
    {
        List<Custom__mdt> customLayoutTypeList = [SELECT DeveloperName 
                                                    FROM Custom__mdt ];
       
 		return customLayoutTypeList; 
           
    }
     
    /* Change name getpickvalplain -> getPlainLayoutTypes()
     * It retrieves plain Layout from Layout custom metadata type
     * Changed - options -> plainLayoutTypeList
	*/
    @AuraEnabled
    public Static  List<Plain__mdt> getPlainLayoutTypes()
    {
		List<Plain__mdt> plainLayoutTypeList = [SELECT DeveloperName 
                                                  FROM Plain__mdt ];
       
 		return plainLayoutTypeList; 
    }


    /* Change name getpickvalconference -> getConferenceTypes()
     * It retrieves conference layout types from Conference types picklist
     * Changed - options -> 
	*/
    @AuraEnabled
    public Static List<String> getConferenceTypes()
    {
        List<String> conferenceTypeList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Venue_Layout__c.Conference_Type__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        
        for (Schema.PicklistEntry value: picklistValues) {
            conferenceTypeList.add(value.getLabel());
        }
        
        return conferenceTypeList;
    }

    /* Change name matchvenuelayout -> 
     * input -> layoutType
	*/
    @AuraEnabled
    public Static List<Venue_Layout__c> matchvenuelayout(Venue_Layout__c layout,Id recordId, String venueid,String layoutType)
    {
        if (layout == null) {
            return null;
        }
        
        String query = 'SELECT id'
                        +' , Conference_Type__c,Horizontal_Seats__c'
                        +' , Layout__c,Layout_Row_Count__c'
                        +' , No_of_Seats__c,No_of_Tables__c'
                        +' , Seats_Per_Row__c,Sections__c,Type__c, Venue__c'
                        +' FROM Venue_Layout__c WHERE Venue__c = :venueid AND Type__c = :layoutType';
        
        system.debug('query'+query);
        
        String layoutconfFields = ' And Conference_Type__c = \''+ layout.Conference_Type__c +'\' AND Horizontal_Seats__c = '+layout.Horizontal_Seats__c + 'AND No_of_Seats__c = '+ layout.No_of_Seats__c+ '';
        String layoutaudienceFields = ' AND Sections__c= '+ layout.Sections__c +' AND No_of_Seats__c = '+layout.No_of_Seats__c +'AND Seats_Per_Row__c= '+layout.Seats_Per_Row__c+ 'AND Layout_Row_Count__c= ' +layout.Layout_Row_Count__c+'';
        String layoutclassroomFields = ' AND No_of_Seats__c ='+ layout.No_of_Seats__c+' AND Seats_Per_Row__c ='+layout.Seats_Per_Row__c+'';
        String layoutBoothFields = ' AND No_of_Seats__c ='+ layout.No_of_Seats__c+' AND Seats_Per_Row__c ='+layout.Seats_Per_Row__c+'';
        
        //Boolean layoutRound = (No_of_Tables__c == layout.No_of_Tables__c);  AND (No_of_Seats__c == layout.No_of_Seats__c);  AND (Layout_Row_Count__c == layout.Layout_Row_Count__c);
        
        List<Venue_Layout__c> queryResult = new List<Venue_Layout__c>();

        layoutType =  layout.Type__c != null ? layout.Type__c.trim() : '';
             system.debug('LAYOUT_TYPE_ROUND'+layout.Type__c);
        if (!String.isEmpty(layoutType) && layout.Type__c.trim().equalsIgnoreCase(LAYOUT_TYPE_ROUND) || (layout.Type__c.trim().equalsIgnoreCase(LAYOUT_TYPE_SQUARE))) {
            system.debug('round');
			String layoutFields = ' AND No_of_Tables__c = '+ layout.No_of_Tables__c +' AND No_of_Seats__c = '+ layout.No_of_Seats__c + ' AND Layout_Row_Count__c = '+ layout.Layout_Row_Count__c + ' ';
            query += layoutFields;
            System.debug('query ==>'+query);
            
            
            // A S K I I: Why are you querying it twice???
            //queryResult = Database.query(query);
            //System.debug(queryResult.size());
            
            
            return Database.query(query);
            //Database.query(query));
        } else if (!String.isEmpty(layoutType) && layoutType.equalsIgnoreCase('ConferenceHall')) {
            query += layoutconfFields;
            System.debug('query ==>'+query);
            
            // A S K I I: Why are you querying it twice???
            //List<Venue_Layout__c> queryResult = Database.query(query);
            //System.debug(queryResult.size());
            //Venue_Layout__c vanueToCreate = new Venue_Layout__c();
            
            return Database.query(query);
            
        } else if (!String.isEmpty(layoutType) && layoutType.equalsIgnoreCase(LAYOUT_TYPE_AUDIENCE) ) {
          	query += layoutaudienceFields;
            System.debug('query ==>'+query);
            
            // A S K I I: Why are you querying it twice???
            //List<Venue_Layout__c> queryResult = Database.query(query);
            //System.debug(queryResult.size());
            //Venue_Layout__c vanueToCreate = new Venue_Layout__c();
           
            return Database.query(query);
            
        } else if (!String.isEmpty(layoutType) && layoutType.equalsIgnoreCase(LAYOUT_TYPE_CLASSROOM) ) {
            query += layoutclassroomFields;
            System.debug('query ==>'+query);
            
            // A S K I I: Why are you querying it twice???
            //List<Venue_Layout__c> queryResult = Database.query(query);
            //System.debug(queryResult.size());
            //Venue_Layout__c vanueToCreate = new Venue_Layout__c();
            
            return Database.query(query);
            
        } else if (!String.isEmpty(layoutType) && layoutType.equalsIgnoreCase(LAYOUT_TYPE_BOOTH) ) {
            query += layoutBoothFields;
            System.debug('query ==>'+query);
            
            // A S K I I: Why are you querying it twice???
            //List<Venue_Layout__c> queryResult = Database.query(query);
            //System.debug(queryResult.size());
            //Venue_Layout__c vanueToCreate = new Venue_Layout__c();
            
            return Database.query(query);
        }
        
		return null;
    }
    
    /* Change name customizedata -> createEventVenueLayout
     * 
	*/
    @AuraEnabled
    public Static  Event_Venue__c createEventVenueLayout(Venue_Layout__c layout, Id recordId, String venueid){
        system.debug('recordId'+recordId);
        if (layout == null) {
            return null;
        }

        String layoutDetailsJson = '';
        DesignLayoutWrapper designLayoutWrapper = new DesignLayoutWrapper();
        designLayoutWrapper.defaultLayoutType = layout.Type__c;
        designLayoutWrapper.guid = generateGUID();
        designLayoutWrapper.layoutRowCount = layout.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 0 ;
        designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
		designLayoutWrapper.SeatsPerRow = designLayoutWrapper.layoutRowCount;//layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;
        designLayoutWrapper.Sections = layout.Sections__c != null ? Integer.valueOf(layout.Sections__c) : 0 ;

        if (layout.Type__c == LAYOUT_TYPE_ROUND || layout.Type__c == LAYOUT_TYPE_SQUARE) {

            designLayoutWrapper.numberOfTables = layout.No_of_Tables__c != null ? Integer.valueOf(layout.No_of_Tables__c) : 0;
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };

            // Creating table layout
            createTableLayout(tableList, layout, designLayoutWrapper);

            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
            designLayoutWrapper.tables.addAll(tableList);

            // create a JSON structure and store in the venue layout
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);

        } else if (layout.Type__c == LAYOUT_TYPE_CLASSROOM || layout.Type__c == LAYOUT_TYPE_BOOTH ) {
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
        
            designLayoutWrapper.layoutRowCount = layout.Layout_Row_Count__c != null ? Integer.valueOf(layout.Layout_Row_Count__c) : 0 ;
            designLayoutWrapper.totalSeats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
            designLayoutWrapper.SeatsPerRow = 1;//designLayoutWrapper.layoutRowCount;//layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;
            designLayoutWrapper.numberOfTables = 1;
        	designLayoutWrapper.SeatsPerRow = 1;    
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = layout.Type__c +'-A'; 
            table.label      = layout.Type__c +'-A';
            table.guid       = generateGUID();
            table.numberOfSeats = designLayoutWrapper.totalSeats;//Integer.valueOf(layout.No_of_Seats__c);
			table.SeatsPerRow = layout.Seats_Per_Row__c != null ? Integer.valueOf(layout.Seats_Per_Row__c) : 1 ;
            designLayoutWrapper.tables.add(table);
            
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
            
        } else if (layout.Type__c == LAYOUT_TYPE_AUDIENCE ) {
            DesignLayoutWrapper.Table[] tableList = new DesignLayoutWrapper.Table[] { };
            DesignLayoutWrapper.Miscellaneous[] miscellaneouslist = new DesignLayoutWrapper.Miscellaneous[] { };
            designLayoutWrapper.tables = new DesignLayoutWrapper.Table[] {};
            designLayoutWrapper.tables.addAll(tableList);
            
            designLayoutWrapper.miscellaneous = new DesignLayoutWrapper.Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
            layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
            
        }/*else if (layout.Type__c == LAYOUT_TYPE_BOOTH ){
             Table[] tableList = new Table[] { };
            Miscellaneous[] miscellaneouslist = new Miscellaneous[] { };
               designLayoutWrapper.tables = new Table[] {};
            designLayoutWrapper.tables.addAll(tableList);
            
              designLayoutWrapper.miscellaneous = new Miscellaneous[] {};
            designLayoutWrapper.miscellaneous.addAll(miscellaneouslist);
                        layoutDetailsJson =  JSON.serialize(designLayoutWrapper);

            System.debug('JSON ::: '+ layoutDetailsJson);
        } */
       
   		 Venue_Layout__c venueLayout = new Venue_Layout__c();
         Event_Venue__c  eventVenue = new Event_Venue__c();

        try {
		
            venueLayout.LayoutDetails__c = layoutDetailsJson;
            venueLayout.Type__c = layout.Type__c;
            venueLayout.Layout__c = layout.Layout__c;
            venueLayout.Venue__c = venueid;
            insert venueLayout;
            
            eventVenue.Campaign__c = recordId;
            eventVenue.Venue_Layout__c = venueLayout.Id;
            eventVenue.Venue__c = venueid;
			insert eventVenue;
           
            system.debug('evVenue'+eventVenue);
        } catch(Exception ex) {
            System.debug('Exception :: ' + ex.getMessage());
        }

        return eventVenue;
    }

    public static void createTableLayout(DesignLayoutWrapper.Table[] tableList, Venue_Layout__c layout, DesignLayoutWrapper designLayoutWrapper) {
        Integer tableCount = 1;
        String alphaSequence = 'A';
        Integer positionX = 200;
        Integer positionY = 200;
        Integer seats = layout.No_of_Seats__c != null ? Integer.valueOf(layout.No_of_Seats__c) : 0;
        Integer tables = layout.No_of_Tables__c != null ? Integer.valueOf(layout.No_of_Tables__c) : 0;
        Integer tableSeatRemainedCount = Math.mod(seats,  tables);
        Integer tableSeatCount = seats / tables;
        System.debug('seats :: ' + seats);
        System.debug('tables :: ' + tables);
        System.debug('tableSeatCount :: ' + tableSeatCount);
        
        
        While (tableCount <= designLayoutWrapper.numberOfTables) {
            DesignLayoutWrapper.Table table = new DesignLayoutWrapper.Table();
            table.layoutType = layout.Type__c;
            table.tableName  = alphaSequence + tableCount;
            table.label      = alphaSequence + tableCount;
            table.guid       = generateGUID();
            
            if (tableSeatRemainedCount > 0 && tableCount == designLayoutWrapper.numberOfTables) {
                table.numberOfSeats = tableSeatCount + tableSeatRemainedCount;//Integer.valueOf(layout.No_of_Seats__c);
            } else {
            	table.numberOfSeats = tableSeatCount;//Integer.valueOf(layout.No_of_Seats__c);    
            }

            if (layout.Type__c == LAYOUT_TYPE_SQUARE) {
                table.height = '';
                table.width  = '';
            }

            DesignLayoutWrapper.Position position = new DesignLayoutWrapper.Position();
            position.positionX = positionX + 'px';
            position.positionY = positionY + 'px';
            table.position = position;

            tableList.add(table);
            tableCount++;
            positionX += 100;
            positionY += 100;
        }
        
        
    }

    public static String generateGUID() {
        return EncodingUtil.convertToHex(Crypto.generateDigest('MD5', Blob.valueOf(DateTime.now().getTime().format())));
    }

    // Create Event venue record: DML
    public static Event_Venue__c createEventVenue(Event_Venue__c eventVenue) {
        try {
            insert eventVenue;
        } catch(Exception e) {
            system.debug('Exception :: '+e);
        }
        
        return eventVenue;
    }

    // Clone and Create Venue Layout if present: DML
    public static Venue_Layout__c getVenueLayout(String venueLayoutId) {
        Venue_Layout__c venueLayout = [ SELECT Id
                                          FROM Venue_Layout__c
                                         WHERE Id = :venueLayoutId
                                         LIMIT 1];
        
        Venue_Layout__c venueLayoutNew = venueLayout != null ? venueLayout.clone() : null;
        
        if (venueLayoutNew == null) {
            return null;
        }
        
        try {
            insert venueLayoutNew;
        } catch(Exception ex) {
            
        }
        
        return venueLayoutNew;
    }

    public  class SelectedVenueWrapper {
    	 @AuraEnabled
        public String Id;
         @AuraEnabled
        public String Name;
         @AuraEnabled
        public String capacity;
         @AuraEnabled
        public String Type;
        @AuraEnabled
         public Boolean IsCustomized = false;
        @AuraEnabled
        public String EventVenueId;
    }

    
    /* public static String getNextChar(String oldChar) {
        String key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZAabcdefghijklmnopqrstuvwxyza';
        Integer index = key.indexOfChar(oldChar.charAt(0));
        return index == -1? null: key.substring(index+1, index+2);
    }*/
    

    /*public class Wrapper {
        @AuraEnabled
        public String Table;
        public String Seats;
        public String RowCount;
        public String ConferenceType;
        public String  HorizontalSeats;
        public String  Noofsection;
        public String  Seatperrow;
    }*/

    /* A S K I I
    public class DesignLayoutWrapper {
        public String defaultLayoutType  { get; set; }
    	Public String guid { get; set; }
    	public Integer numberOfTables { get; set; }
    	public Table[] tables { get; set; }
        public Integer layoutRowCount { get; set; }

    	// total seat count
    	public Integer totalSeats  { get; set; }
    	// Seats per row for audience
    	public Integer SeatsPerRow { get; set; }
        
        public Integer Sections { get; set; }
    	// Starting sequence for a Seat, can be alphabetical
    	public String SeatSequenceStart  { get; set; }
    	// Position for the layout
    	Public Position position  { get; set; }

    	// This map will be used to map seats for a table based on registration and layout rendering purpose
    	public Map<String, Event_Registration__c> seatToObjectMap;
        
        public Miscellaneous[] miscellaneous { get; set; }
    }
    

    Public class Table {
        public String layoutType { get; set; }
        public String tableName { get; set; }
        public Integer SeatsPerRow { get; set; }
        
        public String label { get; set; }
        public String guid { get; set; }
        public Integer numberOfSeats { get; set; }
        public Position position { get; set; }
        public String height { get; set; }
        public String width { get; set; }
    }

    public class Position {
    	public String positionX {get; set; }
    	public String positionY {get; set; }
    }
*/

    /*
     Public class Miscellaneous {
         public String objectType{ get; set; }
           public String guid { get; set; }
          public Position position { get; set; }
         
     }*/
    
}